//! Yggdrasil is a cross-platform application development framework intended to provide
//! a set of black-box tools for creating rich experiences that run at native or near-native speeds
//! across all targets.

#![warn(
    anonymous_parameters,
    bare_trait_objects,
    elided_lifetimes_in_paths,
    missing_docs,
    trivial_numeric_casts,
    unreachable_pub,
    unused_extern_crates,
    unused_import_braces,
    unused_qualifications
)]

use vitruvia::graphics_2d;
use vitruvia::graphics_2d::{
    ContextGraphics, ContextualGraphics, Frame, Graphics, ImageRepresentation, Rasterizable,
    Rasterizer, Transform, Vector,
};
use vitruvia::input::Source;
use vitruvia::text::Text;

#[macro_use]
extern crate stdweb;

use std::borrow::Cow;
use std::cell::RefCell;
use std::rc::Rc;

/// Provides interfaces for creating user interface content.
pub mod ui;

pub mod animation;

use ui::{Component, Rendered};

/// An application context.
pub struct Context<T, U> {
    context: T,
    root: U,
    app_ref: Rendering,
}

struct RenderingState {
    measure: Box<dyn Fn(Text) -> Vector>,
    viewport: Box<dyn Fn() -> Vector>,
    rasterize: Box<dyn Fn(Rasterizable, Vector) -> Box<dyn ImageRepresentation>>,
    mouse_position: Vector,
}

#[derive(Clone)]
pub struct Rendering {
    state: Rc<RefCell<RenderingState>>,
    pub hovered: bool,
}

impl Rendering {
    fn new<F, G, H>(measure: F, viewport: G, rasterize: H) -> Rendering
    where
        F: Fn(Text) -> Vector + 'static,
        G: Fn() -> Vector + 'static,
        H: Fn(Rasterizable, Vector) -> Box<dyn ImageRepresentation> + 'static,
    {
        Rendering {
            state: Rc::new(RefCell::new(RenderingState {
                measure: Box::new(measure),
                viewport: Box::new(viewport),
                rasterize: Box::new(rasterize),
                mouse_position: Vector::default(),
            })),
            hovered: false,
        }
    }
    pub fn measure(&self, input: Text) -> Vector {
        (self.state.borrow().measure)(input)
    }
    pub fn viewport(&self) -> Vector {
        (self.state.borrow().viewport)()
    }
    pub fn rasterize(&self, input: Rasterizable, size: Vector) -> Box<dyn ImageRepresentation> {
        (self.state.borrow().rasterize)(input, size)
    }
    fn set_hovered(mut self, hovered: bool) -> Self {
        self.hovered = hovered;
        self
    }
    fn set_mouse_position(&self, position: Vector) {
        self.state.borrow_mut().mouse_position = position;
    }
    pub fn mouse_position(&self) -> Vector {
        self.state.borrow().mouse_position
    }
}

fn concretize(
    inputs: &mut [Rendered<'_>],
    app_ref: Rendering,
    current_transform: Transform,
) -> Vec<Rasterizable> {
    inputs
        .iter_mut()
        .flat_map(|input| match input {
            Rendered::Transform { content, transform } => {
                let mut t = current_transform;
                t.transform(*transform);
                let mut content = concretize(content, app_ref.clone(), t);
                content
                    .iter_mut()
                    .for_each(|entity| entity.transform(*transform));
                content
            }
            Rendered::Object(object) => object.render().into_owned(),
            Rendered::Component(component) => {
                let mut aref = app_ref.clone();
                if component.binds_hover() {
                    let size = component.size();
                    let x = current_transform.position.x;
                    let y = current_transform.position.y;
                    let w = size.x;
                    let h = size.y;
                    let pos = app_ref.mouse_position();
                    if x <= pos.x && pos.x <= x + w && y <= pos.y && pos.y <= y + h {
                        aref = aref.set_hovered(true);
                    }
                }
                concretize(&mut component.render(aref.clone()), aref, current_transform)
            }
        })
        .collect()
}

/// An application.
pub trait App {
    /// Renders the user-visible contents of the application as some [Component].
    fn render(&mut self) -> &mut dyn Component;
}

impl<T, U> Context<T, U>
where
    T: ContextGraphics + 'static,
    U: Frame + 'static,
{
    /// Begins context execution using the provided application.
    pub fn run(mut self, app: Box<dyn App>) {
        let app_ref = self.app_ref.clone();
        let nref = app_ref.clone();
        self.context.mouse().bind(move |event| {
            nref.set_mouse_position(event.position);
        });
        let app_cell = RefCell::new(app);
        self.root.add(move || {
            Cow::from(concretize(
                app_cell
                    .borrow_mut()
                    .render()
                    .render(app_ref.clone())
                    .as_mut(),
                app_ref.clone(),
                Transform::default(),
            ))
        })
    }
}

/// Creates a new application context.
pub fn new() -> Context<impl ContextGraphics, impl Frame> {
    let gfx = graphics_2d::new();
    let root = gfx.frame();
    let measurement_root = root.clone();
    let viewport_root = measurement_root.clone();
    let rasterize_context = gfx.clone();
    Context {
        context: gfx.run(root.clone()),
        root,
        app_ref: Rendering::new(
            move |text| measurement_root.measure(text),
            move || viewport_root.get_size(),
            move |rasterizable, size| rasterize_context.rasterize(rasterizable, size),
        ),
    }
}
