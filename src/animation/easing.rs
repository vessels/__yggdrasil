pub type EasingFn = Box<dyn Fn(f64) -> f64>;

pub fn linear() -> EasingFn {
    Box::new(|p| p)
}

pub fn cubic() -> EasingFn {
    Box::new(|p| p * p * p)
}
