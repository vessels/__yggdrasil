use super::easing::{linear, EasingFn};

pub trait Tweenable {
    fn interpolate(&self, target: &Self, factor: f64) -> Self;
    fn tween(self, target: Self) -> Tween<Self>
    where
        Self: Sized;
}

impl Tweenable for f64 {
    fn interpolate(&self, target: &f64, factor: f64) -> f64 {
        (self + ((target - self) * factor))
    }
    fn tween(self, target: f64) -> Tween<f64> {
        Tween::new(self, target)
    }
}

pub struct Tween<T: Tweenable> {
    inner: T,
    target: T,
    progress: f64,
    easing_fn: EasingFn,
}

impl<T: Tweenable> Tween<T> {
    pub fn new(inner: T, target: T) -> Tween<T> {
        Tween {
            inner,
            target,
            progress: 0.,
            easing_fn: linear(),
        }
    }
    pub fn tick(&mut self, progress: f64) {
        self.progress = (self.progress + progress).min(1.);
    }
    pub fn sample(&self) -> T {
        self.inner
            .interpolate(&self.target, (self.easing_fn)(self.progress))
    }
    pub fn with_easing(mut self, func: EasingFn) -> Self {
        self.easing_fn = func;
        self
    }
    pub fn retarget(&mut self, target: T) {
        self.inner = self.sample();
        self.progress = 0.;
        self.target = target;
    }
}
