use vitruvia::graphics_2d::{Object, Rasterizable, Vector, RGBA8};
use vitruvia::path::Primitive;

use crate::ui::{Component, Rendered};
use crate::Rendering;

pub struct Card {
    contents: Box<dyn Component>,
    alpha: u8,
    radius: f64,
}

impl Card {
    pub fn new(contents: Box<dyn Component>, alpha: u8) -> Box<Card> {
        Box::new(Card {
            contents,
            alpha,
            radius: 0.,
        })
    }
    pub fn set_radius(&mut self, radius: f64) {
        self.radius = radius;
    }
}

impl Component for Card {
    fn render(&mut self, _app_ref: Rendering) -> Vec<Rendered<'_>> {
        let card: Rasterizable = Primitive::rounded_rectangle(self.contents.size(), self.radius)
            .fill(RGBA8::black().with_alpha(self.alpha).into())
            .finalize()
            .into();
        let card: Object = card.into();
        let mut rendered = vec![card.into()];
        let contents: Vec<Rendered<'_>> = vec![Rendered::Component(self.contents.as_mut())];
        rendered.extend(contents);
        rendered
    }
    fn size(&self) -> Vector {
        self.contents.size()
    }
    fn binds_hover(&self) -> bool {
        false
    }
}
