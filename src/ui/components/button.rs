use crate::animation::easing::cubic;
use crate::animation::tween::{Tween, Tweenable};
use crate::ui::components::{Card, Label};
use crate::ui::layout::Margins;
use crate::ui::{Component, Rendered};

use crate::Rendering;

use vitruvia::graphics_2d::Vector;

pub struct Button {
    contents: Box<Card>,
    radius: Tween<f64>,
    hovered: bool,
}

impl Button {
    pub fn new(label: &str) -> Box<Button> {
        Box::new(Button {
            contents: Card::new(
                Margins::new(Label::new(label))
                    .with_uniform(12.)
                    .with_sides(16.),
                220,
            ),
            radius: (0.).tween(0.).with_easing(cubic()),
            hovered: false,
        })
    }
}

impl Component for Button {
    fn render(&mut self, app_ref: Rendering) -> Vec<Rendered<'_>> {
        if app_ref.hovered {
            if !self.hovered {
                self.radius.retarget(self.contents.size().y / 2.);
                self.hovered = true;
            }
        } else if self.hovered {
            self.radius.retarget(0.);
            self.hovered = false;
        }
        self.radius.tick(0.2);
        self.contents.set_radius(self.radius.sample());
        vec![Rendered::Component(self.contents.as_mut())]
    }
    fn size(&self) -> Vector {
        self.contents.size()
    }
    fn binds_hover(&self) -> bool {
        true
    }
}
