use vitruvia::graphics_2d::{ImageRepresentation, Object, StaticObject, Vector, RGBA8};
use vitruvia::text::Text;

use std::cell::{Cell, RefCell};

use crate::ui::{Component, Rendered};
use crate::Rendering;

/// A label.
pub struct Label {
    content: Text,
    size: Cell<Option<Vector>>,
    raster_content: RefCell<Option<Box<dyn ImageRepresentation>>>,
}

impl Label {
    /// Creates a new label with the provided content.
    pub fn new(content: &str) -> Box<Self> {
        Box::new(Label {
            content: Text::new(content)
                .with_color(RGBA8::white().with_alpha(220))
                .with_size(12),
            raster_content: RefCell::new(None),
            size: Cell::new(None),
        })
    }
}

impl Component for Label {
    fn render(&mut self, app_ref: Rendering) -> Vec<Rendered<'_>> {
        if self.size.get().is_none() {
            self.size.set(Some(app_ref.measure(self.content.clone())));
            *self.raster_content.borrow_mut() =
                Some(app_ref.rasterize(self.content.clone().into(), self.size.get().unwrap()));
        }
        vec![Object::Static(Box::new(StaticObject::from(
            self.raster_content.borrow().as_ref().unwrap().clone(),
        )))
        .into()]
    }
    fn size(&self) -> Vector {
        self.size.get().unwrap_or_default()
    }
    fn binds_hover(&self) -> bool {
        false
    }
}

/// A paragraph.
pub struct Paragraph {
    content: Text,
    size: Cell<Option<Vector>>,
    raster_content: RefCell<Option<Box<dyn ImageRepresentation>>>,
}

impl Paragraph {
    /// Creates a new paragraph with the provided content.
    pub fn new(content: &str, max_width: f64) -> Box<Self> {
        Box::new(Paragraph {
            content: Text::new(content)
                .with_color(RGBA8::black().with_alpha(220))
                .with_max_width(max_width as u32)
                .wrap(),
            size: Cell::new(None),
            raster_content: RefCell::new(None),
        })
    }
}

impl Component for Paragraph {
    fn render(&mut self, app_ref: Rendering) -> Vec<Rendered<'_>> {
        if self.size.get().is_none() {
            self.size.set(Some(app_ref.measure(self.content.clone())));
            *self.raster_content.borrow_mut() =
                Some(app_ref.rasterize(self.content.clone().into(), self.size.get().unwrap()));
        }
        vec![Object::Static(Box::new(StaticObject::from(
            self.raster_content.borrow().as_ref().unwrap().clone(),
        )))
        .into()]
    }
    fn size(&self) -> Vector {
        self.size.get().unwrap_or_default()
    }
    fn binds_hover(&self) -> bool {
        false
    }
}
