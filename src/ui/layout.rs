use crate::ui::{Component, Rendered};

use crate::Rendering;

use vitruvia::graphics_2d::{Transform, Vector};

pub struct Container {
    content: Box<dyn Component>,
    size: Vector,
}

impl Container {
    pub fn new(content: Box<dyn Component>) -> Box<Container> {
        Box::new(Container {
            content,
            size: Vector::default(),
        })
    }
}

impl Component for Container {
    fn render(&mut self, app_ref: Rendering) -> Vec<Rendered<'_>> {
        self.size = app_ref.viewport();
        let content_size = self.content.size();
        let offset = (self.size - content_size) / 2.;
        vec![Rendered::Transform {
            content: vec![Rendered::Component(self.content.as_mut())],
            transform: Transform::default().with_position(offset),
        }]
    }
    fn size(&self) -> Vector {
        self.size
    }
    fn binds_hover(&self) -> bool {
        false
    }
}

pub struct Column {
    content: Vec<Box<dyn Component>>,
    spacing: f64,
}

impl Column {
    pub fn new(content: Vec<Box<dyn Component>>, spacing: f64) -> Box<Column> {
        Box::new(Column { content, spacing })
    }
}

impl Component for Column {
    fn render(&mut self, _app_ref: Rendering) -> Vec<Rendered<'_>> {
        let mut accu = 0.;
        let spacing = self.spacing;
        self.content
            .iter_mut()
            .enumerate()
            .map(|(index, content)| {
                if index != 0 {
                    accu += spacing;
                }
                let content_size = content.size().y;
                let rendered = Rendered::Transform {
                    content: vec![Rendered::Component(content.as_mut())],
                    transform: Transform::default().with_position((0., accu)),
                };
                accu += content_size;
                rendered
            })
            .collect()
    }
    fn size(&self) -> Vector {
        let y = self
            .content
            .iter()
            .map(|content| content.size().y)
            .sum::<f64>()
            + f64::from((self.content.len() - 1).max(0) as u32) * self.spacing;
        let x = self
            .content
            .iter()
            .map(|content| content.size().x as u32)
            .max()
            .unwrap_or(0);
        (f64::from(x), y).into()
    }
    fn binds_hover(&self) -> bool {
        false
    }
}

pub struct Row {
    content: Vec<Box<dyn Component>>,
    spacing: f64,
}

impl Row {
    pub fn new(content: Vec<Box<dyn Component>>, spacing: f64) -> Box<Row> {
        Box::new(Row { content, spacing })
    }
}

impl Component for Row {
    fn render(&mut self, _app_ref: Rendering) -> Vec<Rendered<'_>> {
        let mut accu = 0.;
        let spacing = self.spacing;
        self.content
            .iter_mut()
            .enumerate()
            .map(|(index, content)| {
                if index != 0 {
                    accu += spacing;
                }
                let content_size = content.size().x;
                let rendered = Rendered::Transform {
                    content: vec![Rendered::Component(content.as_mut())],
                    transform: Transform::default().with_position((accu, 0.)),
                };
                accu += content_size;
                rendered
            })
            .collect()
    }
    fn size(&self) -> Vector {
        let x = self
            .content
            .iter()
            .map(|content| content.size().x)
            .sum::<f64>()
            + f64::from((self.content.len() - 1).max(0) as u32) * self.spacing;
        let y = self
            .content
            .iter()
            .map(|content| content.size().y as u32)
            .max()
            .unwrap_or(0);
        (x, f64::from(y)).into()
    }
    fn binds_hover(&self) -> bool {
        false
    }
}

#[derive(Default)]
pub struct Sides {
    pub top: f64,
    pub left: f64,
    pub right: f64,
    pub bottom: f64,
}

pub struct Margins {
    dimensions: Sides,
    content: Box<dyn Component>,
}

impl Sides {
    pub fn uniform(width: f64) -> Sides {
        Sides {
            top: width,
            left: width,
            right: width,
            bottom: width,
        }
    }
    pub fn with_sides(mut self, width: f64) -> Sides {
        self.left = width;
        self.right = width;
        self
    }
    pub fn with_top(mut self, top: f64) -> Sides {
        self.top = top;
        self
    }
    pub fn with_bottom(mut self, bottom: f64) -> Sides {
        self.bottom = bottom;
        self
    }
    pub fn with_left(mut self, left: f64) -> Sides {
        self.left = left;
        self
    }
    pub fn with_right(mut self, right: f64) -> Sides {
        self.right = right;
        self
    }
    fn top_left(&self) -> Vector {
        (self.left, self.top).into()
    }
    fn size(&self) -> Vector {
        (self.left + self.right, self.top + self.bottom).into()
    }
}

impl Margins {
    pub fn new(content: Box<dyn Component>) -> Box<Margins> {
        Box::new(Margins {
            dimensions: Sides::default(),
            content,
        })
    }
    pub fn with_uniform(mut self: Box<Self>, width: f64) -> Box<Margins> {
        self.dimensions = Sides::uniform(width);
        self
    }
    pub fn with_sides(mut self: Box<Self>, width: f64) -> Box<Margins> {
        self.dimensions.left = width;
        self.dimensions.right = width;
        self
    }
}

impl Component for Margins {
    fn render(&mut self, _app_ref: Rendering) -> Vec<Rendered<'_>> {
        vec![Rendered::Transform {
            content: vec![Rendered::Component(self.content.as_mut())],
            transform: Transform::default().with_position(self.dimensions.top_left()),
        }]
    }
    fn size(&self) -> Vector {
        self.content.size() + self.dimensions.size()
    }
    fn binds_hover(&self) -> bool {
        false
    }
}
