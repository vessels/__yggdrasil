use crate::Rendering;
use vitruvia::graphics_2d::{Object, Transform, Vector};

/// Provides assorted useful UI controls and components.
pub mod components;

/// Provides types to facilitate common content layouts.
pub mod layout;

/// Represents the result of rendering a component.
pub enum Rendered<'a> {
    /// Represents a concrete graphics object as the render output.
    Object(Object),
    /// Represents a component as the render output.
    Component(&'a mut dyn Component),
    /// Represents a transform of further output as the render output.
    Transform {
        content: Vec<Rendered<'a>>,
        transform: Transform,
    },
}

/// A UI component.
pub trait Component {
    /// Renders the component into either a concrete graphics object or another component.
    fn render(&mut self, app_ref: Rendering) -> Vec<Rendered<'_>>;
    fn size(&self) -> Vector;
    fn binds_hover(&self) -> bool;
}

impl<'a> From<Object> for Rendered<'a> {
    fn from(input: Object) -> Rendered<'a> {
        Rendered::Object(input)
    }
}
