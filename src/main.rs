use yggdrasil::ui::components::{Button, Card, Paragraph};
use yggdrasil::ui::layout::{Column, Container, Margins, Row};
use yggdrasil::ui::Component;
use yggdrasil::App;

#[macro_use]
extern crate stdweb;

use stdweb::unstable::TryInto;

static IPSUM: &'static str = "I'd just like to interject for a moment. What you’re referring to as Linux, is in fact, GNU/Linux, or as I’ve recently taken to calling it, GNU plus Linux. Linux is not an operating system unto itself, but rather another free component of a fully functioning GNU system made useful by the GNU corelibs, shell utilities and vital system components comprising a full OS as defined by POSIX. Many computer users run a modified version of the GNU system every day, without realizing it. Through a peculiar turn of events, the version of GNU which is widely used today is often called “Linux”, and many of its users are not aware that it is basically the GNU system, developed by the GNU Project. There really is a Linux, and these people are using it, but it is just a part of the system they use. Linux is the kernel: the program in the system that allocates the machine’s resources to the other programs that you run. The kernel is an essential part of an operating system, but useless by itself; it can only function in the context of a complete operating system. Linux is normally used in combination with the GNU operating system: the whole system is basically GNU with Linux added, or GNU/Linux. All the so-called “Linux” distributions are really distributions of GNU/Linux.";

fn create_card() -> Box<dyn Component> {
    Card::new(
        Margins::new(Column::new(
            vec![
                Paragraph::new(
                    &IPSUM
                        .split(' ')
                        .take(
                            js! {
                                return Math.floor(Math.random() * (20 - 5)) + 5;
                            }
                            .try_into()
                            .unwrap(),
                        )
                        .collect::<Vec<&str>>()
                        .join(" "),
                    200.,
                ),
                Button::new("Test Button"),
            ],
            20.,
        ))
        .with_uniform(20.),
        15,
    )
}

fn create_column() -> Box<dyn Component> {
    Column::new((0..3).map(|_| create_card()).collect(), 30.)
}

pub struct TestApp {
    content: Box<dyn Component>,
}

impl TestApp {}

impl App for TestApp {
    fn render(&mut self) -> &mut dyn Component {
        self.content.as_mut()
    }
}

impl TestApp {
    fn new() -> Box<Self> {
        Box::new(TestApp {
            content: Container::new(
                Margins::new(Row::new((0..3).map(|_| create_column()).collect(), 30.))
                    .with_uniform(30.),
            ),
        })
    }
}

fn main() {
    let ctx = yggdrasil::new();
    let app = TestApp::new();
    ctx.run(app);
}
